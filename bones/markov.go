package bones

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/PullRequestInc/go-gpt3"
	"github.com/bwmarrin/discordgo"
	"github.com/mb-14/gomarkov"
	"github.com/tidwall/gjson"
)

var Brain *gomarkov.Chain

func InitializeBrain() {
	Brain = gomarkov.NewChain(3)
	if c, err := BrainLoad("skele.brain"); err != nil {
		BrainTrain("training.txt", Brain)
	} else {
		Brain = c
	}
}

func ToggleLearning() string {
	Config.Learning = !Config.Learning
	if Config.Learning {
		return "Brain toggled on."
	}
	return "Brain toggled off."
}

func IsLearningChannel(check string) bool {
	for _, channel := range Config.TrainingChannels {
		if channel == check {
			return true
		}
	}
	return false
}

func IsMarkovCommand(check string) bool {
	return strings.HasPrefix(check, Config.Prefix+"markov")
}

func BrainLearn(input string) {
	Brain.Add(strings.Split(input, " "))
}

func BrainTrain(name string, c *gomarkov.Chain) error {
	file, err := os.Open(name)
	if err != nil {
		panic(err)
	}
	defer file.Close()

	scanner := bufio.NewScanner(file)
	for scanner.Scan() {
		c.Add(strings.Split(scanner.Text(), " "))
	}
	return nil
}

func BrainLoad(name string) (*gomarkov.Chain, error) {
	var chain gomarkov.Chain
	data, err := os.ReadFile(name)
	if err != nil {
		return &chain, err
	}
	err = json.Unmarshal(data, &chain)
	if err != nil {
		return &chain, err
	}
	return &chain, nil
}

func BrainSave(name string, c *gomarkov.Chain) {
	jsonObj, _ := json.Marshal(c)
	err := os.WriteFile(name, jsonObj, 0644)
	if err != nil {
		fmt.Println(err)
	}
}

func BrainGenerateResponse() string {
	tokens := []string{gomarkov.StartToken}
	for tokens[len(tokens)-1] != gomarkov.EndToken {
		next, _ := Brain.Generate(tokens[(len(tokens) - 1):])
		tokens = append(tokens, next)
	}
	return strings.Join(tokens[1:len(tokens)-1], " ")
}

func Greentext(input string) string {
	ctx := context.Background()
	client := gpt3.NewClient(Config.OpenAI)
	prompt := []string{"write a 4chan greentext about " + input + ":\n\n>"}
	stops := []string{":"}
	temperature := float32(0.7)
	presencepenalty := float32(0.0)
	topp := float32(1.0)
	Logging(Config, Session, AI, "Running greentext request for: "+input)
	resp, err := client.CompletionWithEngine(ctx, "text-davinci-003", gpt3.CompletionRequest{
		Prompt:          prompt,
		MaxTokens:       gpt3.IntPtr(256),
		Stop:            stops,
		TopP:            &topp,
		Temperature:     &temperature,
		PresencePenalty: presencepenalty,
	})
	if err != nil {
		log.Fatalln(err)
	}
	if resp.Choices[0].Text == "" {
		Logging(Config, Session, WARN, "OpenAI did not respond with anything.")
		return "fart haha"
	}
	response := "```>"
	for _, poo := range resp.Choices {
		if poo.Text != "" && poo.Text != "\n" && poo.Text != ">" && poo.Text != ">\n" {
			response = response + poo.Text + "\n"
		}
	}
	response += "```"
	Logging(Config, Session, AI, "Returning greentext!")
	return response
}

func GetGreentextEmbed(topic string) discordgo.MessageEmbed {
	embedded := discordgo.MessageEmbed{
		Description: Greentext(topic),
		Title:       topic,
	}

	return embedded
}

func GPT3(input string) discordgo.MessageEmbed {
	ctx := context.Background()
	client := gpt3.NewClient(Config.OpenAI)
	prompt := []string{input}
	stops := []string{":"}
	temperature := float32(0.7)
	topp := float32(1.0)
	Logging(Config, Session, AI, "Running GPT-3 request for: "+input)
	resp, err := client.CompletionWithEngine(ctx, "text-davinci-003", gpt3.CompletionRequest{
		Prompt:           prompt,
		MaxTokens:        gpt3.IntPtr(256),
		Stop:             stops,
		TopP:             &topp,
		Temperature:      &temperature,
		PresencePenalty:  0.9,
		FrequencyPenalty: 0.57,
	})
	if err != nil {
		log.Fatalln(err)
	}
	message := discordgo.MessageEmbed{}
	message.Footer = &discordgo.MessageEmbedFooter{
		Text: "prompt: " + input,
	}
	if resp.Choices[0].Text == "" {
		Logging(Config, Session, WARN, "OpenAI did not respond with anything.")
		message.Description = "Error processing request with GPT-3."
		return message
	}
	response := ""
	for _, poo := range resp.Choices {
		response = response + poo.Text + "\n"
	}
	message.Description = response
	Logging(Config, Session, AI, "Returning GPT-3 clean response!")
	return message
}

func DALLEImage(input string) discordgo.MessageEmbed {
	Logging(Config, Session, AI, "Running DALL-E image request for: "+input)
	openAIUrl := "https://api.openai.com/v1/images/generations"

	payload := strings.NewReader("{\n\t\"prompt\": \"" + input + "\",\n\t\"n\": 1,\n\t\"size\": \"1024x1024\"\n}")

	req, _ := http.NewRequest("POST", openAIUrl, payload)

	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+Config.OpenAI)

	res, _ := http.DefaultClient.Do(req)

	defer res.Body.Close()
	body, _ := io.ReadAll(res.Body)

	url := gjson.Get(string(body), "data.0.url").String()
	description := ""
	if url == "" {
		description = "Error getting image (0)."
	}
	message := discordgo.MessageEmbed{
		Title: input,
		Image: &discordgo.MessageEmbedImage{
			URL: url,
		},
		Description: description,
		Footer: &discordgo.MessageEmbedFooter{
			Text: "This cost Dr. Skelebones money. Do not abuse.",
		},
	}
	Logging(Config, Session, AI, "Returning DALL-E image!")
	return message
}

func TextToImage(input string) discordgo.MessageEmbed {
	Logging(Config, Session, AI, "Generating image from: "+input)
	url := "https://api.deepai.org/api/text2img"
	payload := strings.NewReader("-----011000010111000001101001\r\nContent-Disposition: form-data; name=\"text\"\r\n\r\n" + input + "\r\n-----011000010111000001101001--\r\n")
	req, _ := http.NewRequest("POST", url, payload)
	req.Header.Add("Api-Key", Config.DeepAI)
	req.Header.Add("Content-Type", "multipart/form-data; boundary=---011000010111000001101001")

	res, _ := http.DefaultClient.Do(req)

	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {

		}
	}(res.Body)
	body, _ := io.ReadAll(res.Body)

	response := discordgo.MessageEmbed{}
	response.Image = &discordgo.MessageEmbedImage{
		URL: gjson.Get(string(body), "output_url").String(),
	}
	response.Title = input

	return response
}
