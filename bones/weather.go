package bones

import (
	"fmt"

	owm "github.com/briandowns/openweathermap"
	"github.com/bwmarrin/discordgo"
)

// GetWeather - gets the weather for a specific location.
func GetWeather(location string) discordgo.MessageEmbed {
	Logging(Config, Session, WEATHER, "Getting weather for `"+location+"`.")
	var message discordgo.MessageEmbed
	// Generate OpenWeatherMap client.
	weather, err := owm.NewCurrent("C", "EN", Config.OpenWeatherMap)
	if err != nil {
		Logging(Config, Session, ERR, "Error making weather client.")
		message.Description = "Error getting weather (1)."
		return message
	}
	// Get weather information.
	err = weather.CurrentByName(location)
	if err != nil {
		Logging(Config, Session, ERR, "Error getting weather for `"+location+"`")
		message.Description = "Error getting weather (2)."
		return message
	}
	Logging(Config, Session, WEATHER, "Generating weather report for `"+location+"`.")
	city := weather.Name
	country := weather.Sys.Country
	// Generate embedded message.
	message = discordgo.MessageEmbed{
		URL:         "https://gitlab.com/drskelebones/skelebot",
		Title:       "Weather by Skele's Choice",
		Color:       6737151,
		Description: "Current weather for " + city + ", " + country + ".",
		Fields: []*discordgo.MessageEmbedField{
			{Name: "Sunrise", Value: fmt.Sprintf("<t:%d>", weather.Sys.Sunrise), Inline: true},
			{Name: "Sunset", Value: fmt.Sprintf("<t:%d>", weather.Sys.Sunset), Inline: true},
			{Name: "Description", Value: weather.Weather[0].Description},
			{Name: "Temperature", Value: fmt.Sprintf("%.2f°C", weather.Main.Temp), Inline: true},
			{Name: "Min Temperature", Value: fmt.Sprintf("%.2f°C", weather.Main.TempMin), Inline: true},
			{Name: "Max Temperature", Value: fmt.Sprintf("%.2f°C", weather.Main.TempMax), Inline: true},
			{Name: "Feels Like", Value: fmt.Sprintf("%.2f°C", weather.Main.FeelsLike), Inline: true},
			{Name: "Humidity", Value: fmt.Sprintf("%d%%", weather.Main.Humidity), Inline: true},
			{Name: "Wind Speed", Value: fmt.Sprintf("%.1f km/h", weather.Wind.Speed), Inline: true},
		},
		Thumbnail: &discordgo.MessageEmbedThumbnail{
			URL: fmt.Sprintf("http://openweathermap.org/img/wn/%s@2x.png", weather.Weather[0].Icon),
		},
		Footer: &discordgo.MessageEmbedFooter{
			Text: "Developed with <3 by Dr. Skelebones of Skele's Choice.",
		},
	}
	return message
}
