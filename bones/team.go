package bones

import (
	"os"
	"strings"

	"github.com/bwmarrin/discordgo"
	"gopkg.in/yaml.v3"
)

type Member struct {
	Name           string   `yaml:"name"`
	Picture        string   `yaml:"picture"`
	Description    string   `yaml:"description"`
	AlternateNames []string `yaml:"alternate_names"`
	Roles          []string `yaml:"roles"`
	Projects       []string `yaml:"projects"`
	Country        string   `yaml:"country"`
	Location       string   `yaml:"location"`
	URL            string   `yaml:"url"`
	Git            string   `yaml:"git"`
	Twitter        string   `yaml:"twitter"`
	Discord        string   `yaml:"discord"`
	Email          string   `yaml:"email"`
	Roblox         string   `yaml:"roblox"`
	YouTube        string   `yaml:"youtube"`
	Steam          string   `yaml:"steam"`
	Itch           string   `yaml:"itch"`
	Kofi           string   `yaml:"kofi"`
	Linkedin       string   `yaml:"linkedin"`
}

type Team struct {
	Members []Member `yaml:"team"`
}

func GetTeamMember(name string) discordgo.MessageEmbed {
	Logging(Config, Session, TEAM, "Getting team member bio for `"+name+"`.")
	bio := discordgo.MessageEmbed{}
	file, err := os.ReadFile("team.yml")
	if err != nil {
		Logging(Config, Session, WARN, "Error opening team file.")
		bio.Description = "Error getting team member (0)."
		return bio
	}

	var team Team
	err = yaml.Unmarshal(file, &team)
	if err != nil {
		Logging(Config, Session, WARN, "Error reading team file.")
		bio.Description = "Error getting team member (1)."
		return bio
	}
	memberIndex := FindTeamMember(team, name)
	if memberIndex == -1 {
		Logging(Config, Session, WARN, "No team member found.")
		bio.Description = "No team member found."
		return bio
	}
	return GenerateBio(team.Members[memberIndex])
}

func GenerateBio(member Member) discordgo.MessageEmbed {
	Logging(Config, Session, TEAM, "Generating bio for "+member.Name)
	message := discordgo.MessageEmbed{
		Title: member.Name,
		Color: 6737151,
		Thumbnail: &discordgo.MessageEmbedThumbnail{
			URL: member.Picture,
		},
		Footer: &discordgo.MessageEmbedFooter{
			Text: "Developed with <3 by Dr. Skelebones of Skele's Choice.",
		},
	}
	if member.URL != "" {
		message.URL = member.URL
	}
	if member.Discord != "" {
		discord := discordgo.MessageEmbedField{Name: "Discord", Value: member.Discord, Inline: true}
		message.Fields = append(message.Fields, &discord)
	}
	if len(member.Roles) > 0 {
		roles := discordgo.MessageEmbedField{Name: "Roles", Inline: true}
		for _, role := range member.Roles {
			if roles.Value == "" {
				roles.Value = "" + role + ", "
			} else {
				roles.Value = roles.Value + role + ", "
			}
		}
		roles.Value = strings.TrimSuffix(roles.Value, ", ")
		message.Fields = append(message.Fields, &roles)
	}
	if member.Country != "" {
		message.Fields = append(message.Fields, &discordgo.MessageEmbedField{Name: "Country", Value: member.Country, Inline: true})
	}
	if member.Location != "" {
		message.Fields = append(message.Fields, &discordgo.MessageEmbedField{Name: "Location", Value: member.Location, Inline: true})
	}
	if member.Twitter != "" {
		message.Fields = append(message.Fields, &discordgo.MessageEmbedField{Name: "Twitter", Value: member.Twitter, Inline: true})
	}
	if member.Email != "" {
		message.Fields = append(message.Fields, &discordgo.MessageEmbedField{Name: "Email", Value: member.Email, Inline: true})
	}
	if member.Linkedin != "" {
		message.Fields = append(message.Fields, &discordgo.MessageEmbedField{Name: "LinkedIn", Value: member.Linkedin, Inline: true})
	}
	if member.Kofi != "" {
		message.Fields = append(message.Fields, &discordgo.MessageEmbedField{Name: "Ko-fi", Value: member.Kofi, Inline: true})
	}
	if member.Steam != "" {
		message.Fields = append(message.Fields, &discordgo.MessageEmbedField{Name: "Steam", Value: member.Steam, Inline: true})
	}
	if member.YouTube != "" {
		message.Fields = append(message.Fields, &discordgo.MessageEmbedField{Name: "YouTube", Value: member.YouTube, Inline: true})
	}
	if member.Roblox != "" {
		message.Fields = append(message.Fields, &discordgo.MessageEmbedField{Name: "Roblox", Value: member.Roblox, Inline: true})
	}
	if member.Git != "" {
		message.Fields = append(message.Fields, &discordgo.MessageEmbedField{Name: "Git", Value: member.Git, Inline: true})
	}
	if member.Itch != "" {
		message.Fields = append(message.Fields, &discordgo.MessageEmbedField{Name: "Itch.io", Value: member.Itch, Inline: true})
	}
	if len(member.Projects) > 0 {
		projects := discordgo.MessageEmbedField{Name: "Projects", Inline: true}
		for _, project := range member.Projects {
			if projects.Value == "" {
				projects.Value = "" + project + ", "
			} else {
				projects.Value = projects.Value + project + ", "
			}
		}
		projects.Value = strings.TrimSuffix(projects.Value, ", ")
		message.Fields = append(message.Fields, &projects)
	}
	return message
}

func FindTeamMember(team Team, name string) int {
	Logging(Config, Session, TEAM, "Getting team member index.")
	for i := 0; i < len(team.Members); i++ {
		member := team.Members[i]
		if member.Name == name || strings.Contains(member.Name, name) || strings.Contains(strings.ToLower(member.Name), name) {
			return i
		}
		for j := 0; j < len(member.AlternateNames); j++ {
			alternateName := member.AlternateNames[j]
			if alternateName == name || strings.Contains(alternateName, name) || strings.Contains(strings.ToLower(alternateName), name) {
				return i
			}
		}
	}
	return -1
}
