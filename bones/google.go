package bones

import (
	"github.com/bwmarrin/discordgo"
	"math/rand"
	"net/http"
	"strings"

	"google.golang.org/api/customsearch/v1"
	"google.golang.org/api/googleapi/transport"
)

// GoogleSearch - returns the first link for a Google search.
func GoogleSearch(query string) discordgo.MessageEmbed {
	// Create the HTTP client.
	client := &http.Client{
		Transport: &transport.APIKey{
			Key: Config.Google,
		},
	}
	// Create the new service object using the HTTP client.
	svc, err := customsearch.New(client)
	message := discordgo.MessageEmbed{
		Footer: &discordgo.MessageEmbedFooter{
			Text: "Nothing found for '" + strings.TrimSpace(query) + "'!",
		},
	}
	if err != nil {
		Logging(Config, Session, ERR, "Error creating Google search client!")
		return message
	}
	// Perform the query.
	resp, err := svc.Cse.List().Cx(Config.CSE).Q(query).Do()
	if err != nil {
		Logging(Config, Session, ERR, "Error handling Google search response!")
		return message
	}
	// Check there are results then return the first link.
	if len(resp.Items) > 0 {
		Logging(Config, Session, GOOGLE, "Returning Google search result for: "+strings.TrimSpace(query))

		message.Description = resp.Items[0].Snippet
		message.Title = resp.Items[0].Title
		message.URL = resp.Items[0].Link
		if resp.Items[0].Image != nil {
			message.Thumbnail = &discordgo.MessageEmbedThumbnail{
				URL: resp.Items[0].Image.ThumbnailLink,
			}
		}
		message.Footer.Text = "Searched for: " + strings.TrimSpace(query)
		return message
	}
	Logging(Config, Session, WARN, "Nothing found for '"+strings.TrimSpace(query)+"'!")
	// If no results then let the user know.
	return message
}

// ImageSearch - basic Google image search.
func ImageSearch(query string) discordgo.MessageEmbed {
	// Create the HTTP client.
	client := &http.Client{Transport: &transport.APIKey{Key: Config.Google}}
	// Create the service object using the HTTP client.
	svc, err := customsearch.New(client)
	message := discordgo.MessageEmbed{
		Footer: &discordgo.MessageEmbedFooter{
			Text: "Nothing found for '" + strings.TrimSpace(query) + "'!",
		},
	}
	if err != nil {
		Logging(Config, Session, ERR, "Error creating Google image search client!")
		return message
	}
	// Perform the image search query.
	resp, err := svc.Cse.List().Cx(Config.CSE).Q(query).SearchType("image").Do()
	if err != nil {
		Logging(Config, Session, ERR, "Error handling Google image search response!")
		return message
	}
	// If there are results then return the link of the first image.
	if len(resp.Items) > 0 {
		Logging(Config, Session, GOOGLE, "Returning Google image search result for: "+strings.TrimSpace(query))
		message.Image = &discordgo.MessageEmbedImage{
			URL: resp.Items[0].Link,
		}
		message.Footer.Text = "Searched for: " + strings.TrimSpace(query)
		return message
	}
	Logging(Config, Session, WARN, "Nothing found by Google for '"+strings.TrimSpace(query)+"'!")
	// If no results then let the user know.
	return message
}

// ImageSearchRandom - basic Google image search but random return!.
func ImageSearchRandom(size int, query string) string {
	// Create the HTTP client.
	client := &http.Client{Transport: &transport.APIKey{Key: Config.Google}}
	// Create the service object using the HTTP client.
	svc, err := customsearch.New(client)
	if err != nil {
		Logging(Config, Session, ERR, "Error creating Google image search client!")
		return "Nothing found for '" + strings.TrimSpace(query) + "'!"
	}
	// Perform the image search query.
	resp, err := svc.Cse.List().Cx(Config.CSE).Q(query).SearchType("image").Do()
	if err != nil {
		Logging(Config, Session, ERR, "Error handling Google image search response!")
		return "Nothing found for '" + strings.TrimSpace(query) + "'!"
	}
	// If there are results then return the link of the first image.
	if len(resp.Items) > 0 {
		Logging(Config, Session, GOOGLE, "Returning Google image search result for: "+strings.TrimSpace(query))
		if len(resp.Items) < size {
			return resp.Items[rand.Intn(len(resp.Items)-1)].Link
		}
		return resp.Items[rand.Intn(size)].Link
	}
	Logging(Config, Session, WARN, "Nothing found by Google for '"+strings.TrimSpace(query)+"'!")
	// If no results then let the user know.
	return "Nothing found for '" + strings.TrimSpace(query) + "'!"
}
