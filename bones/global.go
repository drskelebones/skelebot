package bones

import (
	"github.com/bwmarrin/discordgo"
	"os"
)

var (
	// Config - Global config
	Config Configuration

	// CringeState - Global Cringe Unified Manager
	CringeState CUM

	// Global variables
	PREFIX  = "."
	Session *discordgo.Session
	Guild   *discordgo.Guild
)

// Global constants
const (
	INFO              = "INFO"
	WARN              = "WARNING"
	ERR               = "ERROR"
	COM               = "COMMAND"
	HELP              = "HELP"
	HTTP              = "HTTP"
	TODO              = "TODOIST"
	YOUTUBE           = "YOUTUBE"
	GOOGLE            = "GOOGLE"
	PHISH             = "PHISH"
	EIGHTBALL         = "8BALL"
	PICK              = "PICK"
	DICE              = "DICE"
	COIN              = "COIN"
	CHOOSE            = "CHOOSE"
	EXPLODE           = "EXPLODE"
	CRINGE            = "CRINGE"
	ANIME             = "ANIME"
	TOUHOU            = "TOUHOU"
	MORSE             = "MORSE"
	WEATHER           = "WEATHER"
	STATUS            = "STATUS"
	SEC               = "SECURITY"
	SEARCH            = "SEARCH"
	TEAM              = "TEAM"
	BRAIN             = "BRAIN"
	AI                = "OPENAI"
	MARKOV            = "MARKOV"
	WOLF              = "WOLFRAM"
	PHISHERMAN_CHECK  = "https://api.phisherman.gg/v2/domains/check/"
	PHISHERMAN_REPORT = "https://api.phisherman.gg/v2/phish/report"
	TODOIST_URL       = "https://api.todoist.com/rest/v1/tasks"
	ANILIST_QUERY     = "{\"query\":\"query ($page: Int $perPage: Int, $search: String) {\\r\\n    Page(page: $page, perPage: $perPage) {\\r\\n        pageInfo {\\r\\n            total\\r\\n            perPage\\r\\n        }\\r\\n        media(search: $search, type: ANIME, sort: FAVOURITES_DESC) {\\r\\n            id\\r\\n            title {\\r\\n                romaji\\r\\n                english\\r\\n                native\\r\\n            }\\r\\n            type\\r\\n            genres\\r\\n        }\\r\\n    }\\r\\n}\""
	APIVOID_URL       = "https://endpoint.apivoid.com/urlrep/v1/pay-as-you-go/"
	SKELE             = `
               Skele's Choice Skelebot
                       ______
                    .-"      "-.
                   /            \
                  |              |
                  |,  .-.  .-.  ,|
                  | )(__/  \__)( |
                  |/     /\     \|
                  (_     ^^     _)
                   \__|IIIIII|__/
                    | \IIIIII/ |
                    \          /
                     '--------'
        Skele's Choice, it's the best choice!
                  Ask your bones!`
	COMMANDS = `
- **(ping | pong)** :: pings bot.
- **help** :: get this output.
- **(todo | task) [todo]** :: make a feature request or report an issue.
- **(yt | youtube) [query]** :: search YouTube for a video.
- **(img | image) [query]** :: search for an image.
- **(g | google) [query]**:: search Google.
- **pcheck [URL]** :: check a URL against phisherman.
- **preport [URL]** :: report a URL to phisherman.
- **8ball** :: shake a magic eightball.
- **(flip | coin)** :: flip a coin.
- **pick [number]** :: pick a number between zero and specified value.
- **(choose | decide) [comma separated list]** :: chooses an item from provided comma separated list.
- **(aeburp | explode | aeurp)** :: see if you explode.
- **(roll | dice) [die specification, i.e. "2d6" for two size sided die]** :: roll die/dice.
- **status** :: get skelebot status.
- **cringe [thing]** :: calculate something's cringe level.
- **(mal | anime) [query]** :: search MyAnimeList.
- **anilist [query]** :: search Anilist.
- **(src | source)** :: get source link.
- **morse [message]** :: encode message to morse.
- **demorse [message]** :: decode message from morse.
- **invidious [query]** :: search Invidious. 
- **(w | weather) [location]** :: weather.
- **(urlcheck | checkurl) [URL]** :: check a URL to see if it's malicious.
- **(2hu | touhou)** :: get a random Touhou image.
- **(skelesearch | answer) [query]** :: search for infos.
- **(skelesolve | math) [math problem]** :: answer math question.
- **(team | developer) [team member]** :: gets a team member card.
- **(puhu | talk | markov)** :: get a generated message from Skelebot's brain.
- **savebrain** :: save's Skelebot's brain.
- **qi** :: get a quote image.
- **chat [query]** :: talk with Skelebot!
	`
)

// Config type

type Configuration struct {
	Prefix             string   `yaml:"prefix"`            // Bot prefix
	Config_File        string   `yaml:"config"`            // Config location
	Token              string   `yaml:"token"`             // Discord API key
	Logging            string   `yaml:"logging"`           // Logging channel ID
	Learning           bool     `yaml:"learning"`          // If learning is enabled or not.
	Todoist            string   `yaml:"todoist"`           // Todoist API key
	Todoist_Project    string   `yaml:"todoist_project"`   // Todoist Project ID
	Phisherman         string   `yaml:"phisherman"`        // Phisherman API key
	Google             string   `yaml:"google"`            // Google API key
	CSE                string   `yaml:"cse"`               // Custom Search Engine ID from Google
	MAL_ID             string   `yaml:"mal_id"`            // MyAnimeList API Client ID
	MAL_Secret         string   `yaml:"mal_secret"`        // MyAnimeList API Client Secret
	Invidious_URL      string   `yaml:"invidious"`         // Invidious URL
	OpenWeatherMap     string   `yaml:"weather"`           // OpenWeatherMap API key
	APIvoid            string   `yaml:"apivoid"`           // APIvoid API key
	Wolfram            string   `yaml:"wolfram"`           // Wolfram API key
	OpenAI             string   `yaml:"openai"`            // OpenAI API key
	DeepAI             string   `yaml:"deepai"`            // DeepAI API key
	HumioURL           string   `yaml:"humio-url"`         // Humio base URL
	HumioUsageToken    string   `yaml:"humio-usage-token"` // Humio ingest token for usage
	HumioLoggingToken  string   `yaml:"humio-log-token"`   // Humio ingest token for logging
	AllowedRoles       []string `yaml:"allowedroles"`      // Allowed Discord roles
	EightballResponses []string `yaml:"8ball"`             // Responses used by 8ball
	Touhou             []string `yaml:"touhou"`            // Touhou search strings
	TrainingChannels   []string `yaml:"training"`          // Training channels for Brain
	QuoteImages        []string `yaml:"quoteimages"`       // Quote images thing for the meme
}

// Interaction types for interaction functions

type InteractionText func() string
type InteractionTextQuery func(string) string
type InteractionFile func(string)
type InteractionFileQuery func(string) os.File
type InteractionEmbed func() discordgo.MessageEmbed
type InteractionEmbedQuery func(string) discordgo.MessageEmbed

// Phisherman type

type Phisherman struct {
	Success        bool   `json:"success"`
	Message        string `json:"message"`
	Classification string `json:"classification"`
	Verified       bool   `json:"verifiedPhish"`
}

// Cringe Unified Manager AI type

type CUM struct {
	Manual bool // Manual state
	Value  int  // Value returned by cringe
}
