package bones

import (
	"bufio"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"sync"

	"github.com/nstratos/go-myanimelist/mal"
	"github.com/texttheater/golang-levenshtein/levenshtein"
	"github.com/tidwall/gjson"
	"golang.org/x/oauth2"
)

// Compare - Used when optimizing the search results.
type Compare struct {
	Index    int // Index of anime in list.
	Distance int // Lowest distance from its main and English titles.
}

const cacheName = "auth-token-cache.txt"

var options levenshtein.Options = levenshtein.Options{
	InsCost: 1,
	DelCost: 2,
	SubCost: 2,
	Matches: levenshtein.IdenticalRunes,
}

// GetTouhou - gets random Touhou character.
func GetTouhou() string {
	Logging(Config, Session, TOUHOU, "Getting random Touhou character (based on config).")
	return ImageSearchRandom(20, Config.Touhou[rand.Intn(len(Config.Touhou))])
}

// AnilistSearch - search Anilist.
func AnilistSearch(input string) string {
	Logging(Config, Session, ANIME, "Searching Anilist for: "+input)
	// Generate payload and pass to HTTP client.
	request := ANILIST_QUERY + `, "variables": { "search": "` + input + `", "page": 1, "perPage": 1 }}`
	payload := strings.NewReader(request)
	client := &http.Client{}
	req, err := http.NewRequest("POST", "https://graphql.anilist.co", payload)
	if err != nil {
		Logging(Config, Session, WARN, "Error generating POST request for Anilist.")
		fmt.Println(err)
		return "Error generating POST request for Anilist."
	}
	// Add headers.
	req.Header.Add("Accept", "application/json")
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Cookie", "laravel_session=HpLvJj5azt4z7Haw6r9xib31gwbB1YvcPdUhDnS1")
	// Perform POST request.
	res, err := client.Do(req)
	if err != nil {
		Logging(Config, Session, WARN, "Error sending POST request to Anilist.")
		return "Nothing returned from Anilist."
	}
	defer res.Body.Close()
	// Handle the response.
	body, err := io.ReadAll(res.Body)
	if err != nil {
		Logging(Config, Session, WARN, "Error processing response from Anilist.")
		return "Error processing response from Anilist."
	}
	// Get infos.
	url := fmt.Sprintf("https://anilist.co/anime/%s", gjson.Get(string(body), "data.Page.media.0.id").String())
	titleRomaji := gjson.Get(string(body), "data.Page.media.0.title.romaji").String()
	titleEnglish := gjson.Get(string(body), "data.Page.media.0.title.english").String()
	titleNative := gjson.Get(string(body), "data.Page.media.0.title.native").String()
	Logging(Config, Session, ANIME, "Found anime. Titles: "+titleEnglish+" | "+titleRomaji+" | "+titleNative)
	// Return the URL.
	return url
}

// MALSearch - search MyAnimeList.
func MALSearch(query string) string {
	Logging(Config, Session, ANIME, "Searching MyAnimeList for: "+query)
	// Grab context.
	ctx := context.Background()
	state := ""
	// Generate the authentication token.
	tokenClient, err := authenticate(ctx, Config.MAL_ID, Config.MAL_Secret, state)
	if err != nil {
		Logging(Config, Session, WARN, "Error generating token client.")
		return "Error generating token client."
	}
	// New MyAnimeList client.
	c := mal.NewClient(tokenClient)
	// Get the list of animes based on the query.
	list, _, err := c.Anime.List(ctx, query, mal.Limit(100), mal.Offset(0))
	if err != nil || len(list) < 1 {
		Logging(Config, Session, WARN, "Error searching MyAnimeList for: "+query)
		log.Println(err)
		return "Error searching MyAnimeList for: " + query
	}
	// Create a slice of Compare type objects.
	comparison := make([]Compare, 0)
	// Creat WaitGroup.
	var wg sync.WaitGroup
	wg.Add(len(list) - 1)
	Logging(Config, Session, ANIME, "Optimizing search results.")
	// Start the parallelization with a for loop.
	for i := 0; i < len(list)-1; i++ {
		// Create a function that takes the index.
		go func(index int) {
			// Let the group know when a thread completes.
			defer wg.Done()
			// Get the alternative (synonyms, English, Japanese) titles for the current anime.
			anime, _, _ := c.Anime.Details(ctx, list[index].ID, mal.Fields{"alternative_titles"})
			// Get the levenshtein distance of the main title from the query.
			mainTitle := levenshtein.DistanceForStrings([]rune(query), []rune(anime.Title), options)
			// Get the levenshtein distance of the English title from the query.
			englishTitle := levenshtein.DistanceForStrings([]rune(query), []rune(anime.AlternativeTitles.En), options)
			// New Compare object for appending.
			var cmp Compare
			// Determine which title to use then append it to the comparison slice.
			if englishTitle < mainTitle {
				cmp.Index = index
				cmp.Distance = englishTitle
				comparison = append(comparison, cmp)
			} else {
				cmp.Index = index
				cmp.Distance = mainTitle
				comparison = append(comparison, cmp)
			}
		}(i)
	}
	// Wait for the above to complete.
	wg.Wait()
	// Closest index for the closest title to the query based on distance from above.
	closest_index := 0
	// Previous distance for comparing.
	prev := comparison[0].Distance
	for i := 0; i < len(comparison); i++ {
		// If the previous distance is greater than the comparative distance, set the closest index and new previous distance.
		if prev > comparison[i].Distance {
			closest_index = comparison[i].Index
			prev = comparison[i].Distance
		}
	}
	// Create the URL.
	url := fmt.Sprintf("https://myanimelist.net/anime/%d", list[closest_index].ID)
	Logging(Config, Session, ANIME, "Found anime at: `"+url+"`")
	// Return the URL :)
	return url
}

func authenticate(ctx context.Context, clientID, clientSecret, state string) (*http.Client, error) {
	// Prepare the oauth2 configuration with your application ID, secret, the
	// MyAnimeList authentication and token URLs as specified in:
	//
	// https://myanimelist.net/apiconfig/references/authorization
	conf := &oauth2.Config{
		ClientID:     clientID,
		ClientSecret: clientSecret,
		Endpoint: oauth2.Endpoint{
			AuthURL:   "https://myanimelist.net/v1/oauth2/authorize",
			TokenURL:  "https://myanimelist.net/v1/oauth2/token",
			AuthStyle: oauth2.AuthStyleInParams,
		},
	}

	oauth2Token, err := loadCachedToken()
	if err == nil {
		refreshedToken, err := conf.TokenSource(ctx, oauth2Token).Token()
		if err == nil && (oauth2Token != refreshedToken) {
			fmt.Println("Caching refreshed oauth2 token...")
			if err := cacheToken(*refreshedToken); err != nil {
				return nil, fmt.Errorf("caching refreshed oauth2 token: %s", err)
			}
			return conf.Client(ctx, refreshedToken), nil
		}
		return conf.Client(ctx, oauth2Token), nil
	}

	// Generate a code verifier, a high-entropy cryptographic random string. It
	// will be set as the code_challenge in the authentication URL. It should
	// have a minimum length of 43 characters and a maximum length of 128
	// characters.
	const codeVerifierLength = 128
	codeVerifier, err := generateCodeVerifier(codeVerifierLength)
	if err != nil {
		return nil, fmt.Errorf("generating code verifier: %v", err)
	}

	// Produce the authentication URL where the user needs to be redirected and
	// allow your application to access their MyAnimeList data.
	authURL := conf.AuthCodeURL(state,
		oauth2.SetAuthURLParam("code_challenge", codeVerifier),
	)

	fmt.Printf("OPEN IN BROWSER: %v\n", authURL)
	fmt.Print("COPY CODE FROM REDIRECT AND PASTE HERE: ")
	scanner := bufio.NewScanner(os.Stdin)
	scanner.Scan()
	code := scanner.Text()
	if scanner.Err() != nil {
		return nil, fmt.Errorf("reading code from terminal: %v", err)
	}

	// Exchange the authentication code for a token. MyAnimeList currently only
	// supports the plain code_challenge_method so to verify the string, just
	// make sure it is the same as the one you entered in the code_challenge.
	token, err := conf.Exchange(ctx, code,
		oauth2.SetAuthURLParam("code_verifier", codeVerifier),
	)
	if err != nil {
		return nil, fmt.Errorf("exchanging code for token: %v", err)
	}
	fmt.Println("Authentication was successful. Caching oauth2 token...")
	if err := cacheToken(*token); err != nil {
		return nil, fmt.Errorf("caching oauth2 token: %s", err)
	}

	return conf.Client(ctx, token), nil
}

func cacheToken(token oauth2.Token) error {
	b, err := json.MarshalIndent(token, "", "   ")
	if err != nil {
		return fmt.Errorf("marshaling token %s: %v", token, err)
	}
	err = os.WriteFile(cacheName, b, 0644)
	if err != nil {
		return fmt.Errorf("writing token %s to file %q: %v", token, cacheName, err)
	}
	return nil
}

func loadCachedToken() (*oauth2.Token, error) {
	b, err := os.ReadFile(cacheName)
	if err != nil {
		return nil, fmt.Errorf("reading oauth2 token from cache file %q: %v", cacheName, err)
	}
	token := new(oauth2.Token)
	if err := json.Unmarshal(b, token); err != nil {
		return nil, fmt.Errorf("unmarshaling oauth2 token: %v", err)
	}
	return token, nil
}

func generateCodeVerifier(length int) (string, error) {
	const charset = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstvuwxyz0123456789-._~"
	bytes := make([]byte, length)
	if _, err := rand.Read(bytes); err != nil {
		return "", err
	}
	for i, b := range bytes {
		bytes[i] = charset[b%byte(len(charset))]
	}
	return string(bytes), nil
}
