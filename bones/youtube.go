package bones

import (
	"errors"
	"io"
	"log"
	"net/url"
	"os"
	"strings"

	"github.com/kkdai/youtube/v2"
	"github.com/raitonoberu/ytsearch"
)

func YouTubeProxy(query string) os.File {
	videoID := YouTubeSearchID(query)
	client := youtube.Client{}
	video, err := client.GetVideo(videoID)
	if err != nil {
		log.Println(err)
	}
	formats := video.Formats.WithAudioChannels() // only get videos with audio
	stream, _, err := client.GetStream(video, &formats[0])
	if err != nil {
		log.Println(err)
	}
	file, err := os.Create("temp.mp4")
	if err != nil {
		log.Println(err)
	}
	_, err = io.Copy(file, stream)
	if err != nil {
		log.Println(err)
	}

	return *file
}

func YouTubeSearchID(query string) string {
	// Search YouTube
	search := ytsearch.VideoSearch(query)
	// Get the first page of results.
	result, err := search.Next()
	if err != nil {
		Logging(Config, Session, WARN, "Error with searching YouTube!")
		return ""
	}
	// Check to see if any results came in.
	if len(result.Videos) > 0 {
		Logging(Config, Session, YOUTUBE, "Found video! Title: "+string(result.Videos[0].Title))
		// Return the URL.
		return result.Videos[0].ID
	}
	Logging(Config, Session, WARN, "Nothing found for '"+query+"'!")
	return ""
}

// YoutubeSearch - takes a query, searches youtube, returns first URL.
func YoutubeSearch(query string) string {
	// Search YouTube
	search := ytsearch.VideoSearch(query)
	// Get the first page of results.
	result, err := search.Next()
	if err != nil {
		Logging(Config, Session, WARN, "Error with searching YouTube!")
	}
	// Check to see if any results came in.
	if len(result.Videos) > 0 {
		Logging(Config, Session, YOUTUBE, "Found video! Title: "+string(result.Videos[0].Title))
		// Return the URL.
		return "https://youtu.be/" + result.Videos[0].ID
	}
	Logging(Config, Session, WARN, "Nothing found for '"+query+"'!")
	return "Nothing found for `" + query + "`!"
}

// GetYouTubeVideoID - gets the video ID out of YouTube links.
func GetYouTubeVideoID(input string) (string, error) {
	Logging(Config, Session, YOUTUBE, "Parsing video ID from: `"+input+"`")
	// Parse the URL.
	u, err := url.Parse(input)
	if err != nil {
		Logging(Config, Session, WARN, "Error parsing YouTube URL.")
		return "Invalid input. Please provide a YouTube URL.", errors.New("Error parsing YouTube URL.")
	}
	// Depending on which type of hostname, parse the video ID and return it.
	switch u.Hostname() {
	case "youtu.be":
		id := strings.TrimPrefix(input, "https://youtu.be/")
		return id, nil
	case "youtube.com", "www.youtube.com":
		params := strings.Split(u.RawQuery, "&")
		id := strings.TrimPrefix(params[0], "v=")
		return id, nil
	default:
		Logging(Config, Session, WARN, "Error parsing YouTube URL.")
		return "Invalid input. Please provide a YouTube URL.", errors.New("Error parsing YouTube URL.")
	}
}

// GetInvidiousURL - takes in a YouTube URL then hands back an Invidious version.
// NOTE: Invidious host must be specified in config.
func GetInvidiousURL(input string) (string, error) {
	Logging(Config, Session, YOUTUBE, "Converting YouTube link to Invidious from: `"+input+"`")
	vid, err := GetYouTubeVideoID(input)
	if err != nil {
		return vid, err
	}
	if Config.Invidious_URL == "" {
		return "No Invidious URL specified in configuration file.", errors.New("No Invidious URL specified in configuration file.")
	}
	Logging(Config, Session, YOUTUBE, "Resultant URL: `"+Config.Invidious_URL+vid+"`")
	return Config.Invidious_URL + vid, nil
}
