package bones

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"math/rand"
	"net/http"
	"net/url"
	"strconv"
	"strings"
	"time"

	"github.com/bwmarrin/discordgo"
	"github.com/hako/durafmt"
	"github.com/shirou/gopsutil/cpu"
	"github.com/shirou/gopsutil/host"
	"github.com/shirou/gopsutil/mem"
)

type Todoist struct {
	Project  string  `json:"project_id"`
	Content  string  `json:"content"`
	Labels   []int64 `json:"label_ids"`
	Priority int     `json:"priority"`
	Due      string  `json:"due_string"`
}

// CringeStateSet - sets cringe state.
func CringeStateSet(input string) string {
	Logging(Config, Session, CRINGE, "Setting cringe output value to: "+input)
	// Get the requested value as an int.
	value, err := strconv.Atoi(input)
	if err != nil {
		Logging(Config, Session, CRINGE, "User did not supply valid input!")
		return "Invalid input. Please supply a number."
	}
	Logging(Config, Session, CRINGE, "User supplied valid input! Setting.")
	// Set the cringe state.
	CringeState.Manual = true
	CringeState.Value = value
	return "Value set successfully. The next Cringe Unified Manager analysis will return " + input + "%"
}

// Cringe - calculates cringe using an AI.
func Cringe(m *discordgo.MessageCreate) {
	Logging(Config, Session, CRINGE, "Running a cringe analysis.")
	// It's all a show.
	Session.ChannelMessageSend(m.ChannelID, "Cringe analysis request accepted from "+m.Author.Username)
	skmessage, _ := Session.ChannelMessageSend(m.ChannelID, "Starting Skele's Choice Cringe Uniform Manager:tm: AI")
	r := rand.Intn(7) + 2
	for i := 0; i < r; i++ {
		skmessage, _ = Session.ChannelMessageEdit(skmessage.ChannelID, skmessage.ID, skmessage.Content+".")
		time.Sleep(time.Second)
	}
	message, _ := Session.ChannelMessageSend(m.ChannelID, "|- CRINGE UNIFORM MANAGER ONLINE.\n|- ANALYZING")
	r = rand.Intn(5) + 1
	for i := 0; i < r; i++ {
		message, _ = Session.ChannelMessageEdit(message.ChannelID, message.ID, message.Content+".")
		time.Sleep(time.Second)
	}
	message, _ = Session.ChannelMessageEdit(message.ChannelID, message.ID, "|- ANALYSIS COMPLETE.\n|- CALCULATING PERCENTAGE.")
	r = rand.Intn(4) + 2
	for i := 0; i < r; i++ {
		message, _ = Session.ChannelMessageEdit(message.ChannelID, message.ID, message.Content+".")
		time.Sleep(time.Second)
	}
	if CringeState.Manual {
		Session.ChannelMessageEdit(message.ChannelID, message.ID, "|- CRINGE PERCENTAGE: "+strconv.Itoa(CringeState.Value)+"%")
		CringeState.Manual = false
		CringeState.Value = 0
	} else {
		Session.ChannelMessageEdit(message.ChannelID, message.ID, "|- CRINGE PERCENTAGE: "+strconv.Itoa(rand.Intn(100))+"%")
	}
	Session.ChannelMessageEdit(skmessage.ChannelID, skmessage.ID, "\nThank you for choosing Skele's Choice!")
}

func GetSourceCode() string {
	return "https://gitlab.com/drskelebones/skelebot"
}

func GetCommandList() string {
	commandList := "```"
	for _, command := range Commands {
		commandList += "- " + command.Name + " :: " + command.Description + "\n"
	}
	commandList += "```"
	return commandList
}

func QuoteImage(s *discordgo.Session, i *discordgo.InteractionCreate) {
	interactionGuild, _ := s.Guild(i.GuildID)
	err := s.InteractionRespond(i.Interaction, &discordgo.InteractionResponse{
		Type: discordgo.InteractionResponseDeferredChannelMessageWithSource,
	})
	if err != nil {
		log.Println(err)
	}
	Usage(Config, COM, i.Interaction.Member.Nick, interactionGuild.Name, "Sending quote image.")
	_, err = s.ChannelMessageSend(i.ChannelID, Config.QuoteImages[rand.Intn(len(Config.QuoteImages))])
	if err != nil {
		log.Println(err)
	}
	err = s.InteractionResponseDelete(i.Interaction)
	if err != nil {
		log.Println(err)
	}
}

// Status - get system status.
// Returns: CPU utilization, memory utilization, host boot time + uptime
func Status() discordgo.MessageEmbed {
	Logging(Config, Session, STATUS, "Getting system stats.")
	// Get CPU info.
	cpuValues, _ := cpu.Percent(time.Second, false)
	cpuUtil := fmt.Sprintf("%.1f", cpuValues[0])
	// Get memory info.
	memValue, _ := mem.VirtualMemory()
	memUtil := fmt.Sprintf("%.1f", memValue.UsedPercent)
	// Get host uptime and boot time.
	hostInfo, _ := host.Info()
	uptime := time.Since(time.Unix(int64(hostInfo.BootTime), 0))
	uptimePretty, _ := durafmt.ParseStringShort(uptime.String())
	bootTime := fmt.Sprintf("<t:%v>", hostInfo.BootTime)
	Logging(Config, Session, STATUS, "Generating status message.")
	// Generate the embedded message using the retrieved values.
	message := discordgo.MessageEmbed{
		URL:   "https://gitlab.com/drskelebones/skelebot",
		Title: "skelebot system status",
		Color: 6737151,
		Fields: []*discordgo.MessageEmbedField{
			{
				Name:   "CPU Utilization",
				Value:  cpuUtil,
				Inline: false,
			},
			{
				Name:   "Memory Utilization",
				Value:  memUtil + "%",
				Inline: true,
			},
			{
				Name:   "Uptime",
				Value:  uptimePretty.String(),
				Inline: true,
			},
			{
				Name:   "Boot Time",
				Value:  bootTime,
				Inline: true,
			},
		},
		Footer: &discordgo.MessageEmbedFooter{
			Text: "Developed with <3 by Dr. Skelebones of Skele's Choice.",
		},
	}
	return message
}

func Coin() string {
	states := []string{"**Heads**", "**Tails**", "**Ricochet**"}
	return "*DING!*... " + states[rand.Intn(len(states))]
}

// Pick - pick a number!
func Pick(n string) string {
	value, _ := strconv.Atoi(n)
	// If the user did not specify a range then default to 10.
	if value != 0 {
		Logging(Config, Session, PICK, "Pick range of 0 to "+n)
		return strconv.Itoa(rand.Intn(value))
	} else {
		Logging(Config, Session, PICK, "No value range specified. Defaulting to 10.")
		return strconv.Itoa(rand.Intn(10))
	}
}

// Choose - choose something
func Choose(input string) string {
	if input == "" {
		Logging(Config, Session, CHOOSE, "User did not provide choices.")
		return "Please provide options to choose from."
	}
	// Pick from the list provided by the user.
	Logging(Config, Session, CHOOSE, "Choosing something from "+strings.TrimPrefix(input, PREFIX+"choose "))
	options := strings.Split(strings.TrimPrefix(input, PREFIX+"choose "), ",")
	choice := options[rand.Intn(len(options))]
	Logging(Config, Session, CHOOSE, "Chose: "+choice)
	return choice
}

// EightBall - magic 8ball command.
func EightBall(question string) string {
	// Select a random response.
	response := Config.EightballResponses[rand.Intn(len(Config.EightballResponses))]
	Logging(Config, Session, EIGHTBALL, "Responding with: "+response)
	return "You asked the Magic Eightball: `" + question + "`\nThe ball says: " + response
}

// Dice - does dice roll.
func Dice(input string) string {
	// Split the input into the necessary parts.
	parts := strings.Split(input, "d")
	if len(parts) != 2 {
		Logging(Config, Session, WARN, "User did not provide valid input.")
		return "Invalid input. Please follow `[# of dice]d[face value of die]`."
	}
	Logging(Config, Session, DICE, "Rolling dice using the following input:"+input)
	// Get the information about the roll in numerical values.
	dieCount, _ := strconv.Atoi(strings.Replace(parts[0], " ", "", -1))
	if dieCount > 20 || dieCount < 1 {
		Logging(Config, Session, WARN, "User requested invalid die configuration!")
		return "Invalid input. Please specify an amount of dice *between 1 and 20*"
	}
	dieFaces, _ := strconv.Atoi(parts[1])
	if dieFaces < 4 {
		Logging(Config, Session, WARN, "User requested invalid die configuration!")
		return "Invalid input. Please specify die faces of *4 or more.*"
	}
	dieSum := 0
	dieResults := make([]string, dieCount)
	// Calculate the roll result.
	for i := 0; i < dieCount; i++ {
		result := rand.Intn(dieFaces-1) + 1
		dieResults[i] = strconv.Itoa(result)
		dieSum += result
	}
	dieResult := strings.Join(dieResults, ", ")
	Logging(Config, Session, DICE, "Roll results: "+dieResult)
	Logging(Config, Session, DICE, "Roll sum: "+strconv.Itoa(dieSum))
	return "**Dice**: " + dieResult + "\n" + "**Sum**: " + strconv.Itoa(dieSum)
}

// PhishermanCheck - check a URL against Phisherman!
// TODO: replace http with req.
func PhishermanCheck(checkUrl string) string {
	// Log.
	Logging(Config, Session, PHISH, "Phisherman check for: `"+checkUrl+"`")
	// Create http client.
	client := &http.Client{}
	// Log.
	Logging(Config, Session, PHISH, "Checking: `"+checkUrl+"`")
	// Generatr report body.
	check := strings.NewReader("")
	// Get domain.
	domain := strings.Replace(checkUrl, " ", "", -1)
	if strings.HasPrefix(strings.TrimSpace(checkUrl), "http") {
		Logging(Config, Session, PHISH, "Extracting domain from URL.")
		u, err := url.Parse(strings.Replace(checkUrl, " ", "", -1))
		if err != nil {
			Logging(Config, Session, PHISH, "Error parsing URL.")
			return "Error parsing URL! Did you submit a valid URL?"
		}
		domain = u.Host
	}
	Logging(Config, Session, PHISH, "Domain: "+domain)
	// Create request.
	req, err := http.NewRequest("PUT", PHISHERMAN_CHECK+domain, check)
	// Check error.
	if err != nil {
		Logging(Config, Session, PHISH, "Error generating Phisherman report!")
		return "Error handling phish report!"
	}
	// Add headers to request.
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+Config.Phisherman)
	// Send request.
	res, err := client.Do(req)
	// Check error.
	if err != nil {
		Logging(Config, Session, PHISH, "Error sending Phisherman report!")
		return "Error handling phish report!"
	}
	// Capture response.
	defer res.Body.Close()
	// Read response.
	body, err := io.ReadAll(res.Body)
	// Check error.
	if err != nil {
		Logging(Config, Session, PHISH, "Error in handling Phisherman check response!")
		return "Error handling phish check!"
	}
	// Unmarshal response.
	var phishermanResponse Phisherman
	err = json.Unmarshal(body, &phishermanResponse)
	if err != nil {
		log.Println(err)
	}
	// Log.
	if phishermanResponse.Verified == true {
		Logging(Config, Session, PHISH, "Phisherman classified the domain as "+phishermanResponse.Classification+" and is verified!")
		return "Phisherman classified the URL as " + phishermanResponse.Classification + " and is verified!"
	} else {
		Logging(Config, Session, PHISH, "Phisherman classified the domain as "+phishermanResponse.Classification+" and is not verified!")
		return "Phisherman classified the URL as " + phishermanResponse.Classification + " and is not verified!"
	}
}

// PhishermanReport - report a URL to Phisherman!
// TODO: replace http with req.
func PhishermanReport(url string) string {
	// Log.
	Logging(Config, Session, PHISH, "Phisherman report for: `"+url+"`")
	// Create http client.
	client := &http.Client{}
	// Log.
	Logging(Config, Session, PHISH, "Reporting the URL: `"+url+"`")
	// Generate report body.
	report := strings.NewReader(`{ "url":"` + strings.Replace(url, " ", "", -1) + `" }`)
	// Create request.
	req, err := http.NewRequest("PUT", PHISHERMAN_REPORT, report)
	// Check error.
	if err != nil {
		Logging(Config, Session, ERR, "Error generating Phisherman report!")
		return "Error handling phish report!"
	}
	// Add headers to request.
	req.Header.Add("Content-Type", "application/json")
	req.Header.Add("Authorization", "Bearer "+Config.Phisherman)
	// Send request.
	res, err := client.Do(req)
	// Check error.
	if err != nil {
		Logging(Config, Session, ERR, "Error sending Phisherman report!")
		return "Error handling phish report!"
	}
	// Capture response.
	defer res.Body.Close()
	// Read response.
	body, err := io.ReadAll(res.Body)
	// Check error.
	if err != nil {
		Logging(Config, Session, ERR, "Error in handling Phisherman report response!")
		return "Error handling phish report!"
	}
	// Unmarshal response.
	var phishermanResponse Phisherman
	err = json.Unmarshal(body, &phishermanResponse)
	if err != nil {
		log.Println(err)
	}
	// Log.
	if phishermanResponse.Success {
		Logging(Config, Session, PHISH, "Phisherman report successful!")
		return "Successful!"
	} else {
		Logging(Config, Session, PHISH, "Phisherman report unsuccessful because: "+phishermanResponse.Message)
		return "Report unsuccessful because: " + phishermanResponse.Message
	}
}
