#!/bin/bash

echo "Thank you for choosing Skele's Choice as your Discord bot provider!"

# Check for skelebot executable
if [ -e ./skelebot ]
then
	echo "[INFO] Skelebot executable found!"
else
	echo "[WARN] Skelebot executable not found!"
	echo "[INFO] Compiling skelebot. Running: go build"
	go build
	echo "[INFO] Ensuring executable has execute permissions!"
	chmod +x ./skelebot
	echo "[INFO] Ready to start!"
fi

if screen -list | grep -i "skelebot"
then
	echo "[WARN] Existing skelebot screen session exists! Terminating!"
	screen -S skelebot -p 0 -X stuff "^C"
fi

echo "[INFO] Starting Skelebot!"
# Start skelebot
#screen -dmS skelebot ./skelebot
pm2 start skelebot

echo "[INFO] Skelebot started! Connect with: screen -r skelebot"
