build: main.go bones/*
	@echo "[INFO] Building skelebot executable."
	go fmt main.go
	go fmt bones/*
	go build
clean: skelebot
	@echo "[WARN] Cleaning skelebot executable."
	rm skelebot
run: skelebot
	@echo "[INFO] Starting skelebot executable."
	./start-skelebot.sh
restart: skelebot
	@echo "[INFO] Restarting skelebot."
	pm2 restart skelebot
	pm2 monit skelebot
fresh: clean build restart
	@echo "[INFO] Starting skelebot with fresh build."
dev: clean build
	@echo "[INFO] Cleaning and building."
	./skelebot
update:
	@echo "[INFO] Pulling latest version of skelebot."
	git pull
update_fresh: update fresh
	@echo "[INFO] Updating skelebot and making a fresh build."