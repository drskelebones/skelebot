module skelebot

go 1.17

require (
	github.com/PullRequestInc/go-gpt3 v1.1.7
	github.com/alwindoss/morse v1.0.1
	github.com/briandowns/openweathermap v0.19.0
	github.com/bwmarrin/discordgo v0.26.1
	github.com/hako/durafmt v0.0.0-20210608085754-5c1018a4e16b
	github.com/imroc/req v0.3.2
	github.com/kkdai/youtube/v2 v2.7.16
	github.com/mb-14/gomarkov v0.0.0-20210216094942-a5b484cc0243
	github.com/nstratos/go-myanimelist v0.9.4
	github.com/raitonoberu/ytsearch v0.2.0
	github.com/shirou/gopsutil v3.21.11+incompatible
	github.com/texttheater/golang-levenshtein v1.0.1
	github.com/tidwall/gjson v1.14.3
	golang.org/x/oauth2 v0.0.0-20221014153046-6fdb5e3db783
	google.golang.org/api v0.99.0
	gopkg.in/yaml.v3 v3.0.1
)

require (
	cloud.google.com/go/compute v1.10.0 // indirect
	github.com/bitly/go-simplejson v0.5.0 // indirect
	github.com/dlclark/regexp2 v1.7.0 // indirect
	github.com/dop251/goja v0.0.0-20220719153422-38a3647bcce0 // indirect
	github.com/go-ole/go-ole v1.2.6 // indirect
	github.com/go-sourcemap/sourcemap v2.1.3+incompatible // indirect
	github.com/golang/groupcache v0.0.0-20210331224755-41bb18bfe9da // indirect
	github.com/golang/protobuf v1.5.2 // indirect
	github.com/google/uuid v1.3.0 // indirect
	github.com/googleapis/enterprise-certificate-proxy v0.2.0 // indirect
	github.com/googleapis/gax-go/v2 v2.6.0 // indirect
	github.com/gorilla/websocket v1.5.0 // indirect
	github.com/tidwall/match v1.1.1 // indirect
	github.com/tidwall/pretty v1.2.1 // indirect
	github.com/tklauser/go-sysconf v0.3.10 // indirect
	github.com/tklauser/numcpus v0.5.0 // indirect
	github.com/yusufpapurcu/wmi v1.2.2 // indirect
	go.opencensus.io v0.23.0 // indirect
	golang.org/x/crypto v0.0.0-20221012134737-56aed061732a // indirect
	golang.org/x/net v0.0.0-20221017152216-f25eb7ecb193 // indirect
	golang.org/x/sys v0.1.0 // indirect
	golang.org/x/text v0.4.0 // indirect
	google.golang.org/appengine v1.6.7 // indirect
	google.golang.org/genproto v0.0.0-20221014213838-99cd37c6964a // indirect
	google.golang.org/grpc v1.50.1 // indirect
	google.golang.org/protobuf v1.28.1 // indirect
)
